import { User, UserService } from './userService';

/**
 * Login service to manage user logins and logouts
 * (currently via local browser storage)
 *
 * @author Tim Wenzel
 */
export class LoginService {

    /**
     * Login user with password
     * @param loginName Username
     * @param password Password
     */
    login(loginName: string, password: string): boolean {
        const user: User | undefined = UserService.findUserByLogin(loginName);

        if (user && user.password === password) {
            UserService.setCurrentUser(user);

            return true;
        }
        return false;
    }

    /**
     * Logout current user
     */
    logout() {
        localStorage.removeItem('currentUser');
    }
}
