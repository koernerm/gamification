import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { User, UserService } from 'src/login/userService';
import { LoginService } from 'src/login/loginService';
import { Battle } from 'src/model/game/battle';

/**
 * Competition tab
 */
@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit {

  /* Name of current player to be displayed on top */
  userString: string;

  constructor(private navCtrl: NavController) {
    const currentUser: User = UserService.getCurrentUser();
    if (currentUser == null) {
      navCtrl.navigateBack('login');
    }
    this.userString = currentUser.firstName + ' ' + currentUser.lastName;

    if (!customElements.get('competition-modal')) {
      this.defineModals();
    }
  }

  /**
   * Initialization of competition view elements
   */
  ngOnInit(): void {
    const users: User[] = UserService.getUsers();
    // Sort users by first name then last name
    users.sort((userA, userB) => userA.firstName.concat(userA.lastName).localeCompare(userB.firstName.concat(userB.lastName)));
    users.forEach(user => {
      if (!(user.loginName === UserService.getCurrentUser().loginName)) {
        this.createUserItem(user);
      }
    });

    const searchbar = document.querySelector('ion-searchbar');
    const items = Array.from(document.querySelectorAll('ion-item'));
    searchbar.addEventListener('ionInput', handleInput);

    function handleInput(event) {
      const query = event.target.value.toLowerCase();
      requestAnimationFrame(() => {
        items.forEach(item => {
          const shouldShow = item.children[0].textContent.toLowerCase().indexOf(query) > -1;
          item.style.display = shouldShow ? 'block' : 'none';
        });
      });
    }
  }

  /**
   * Definition of competition overlay
   */
  private defineModals() {
    customElements.define('competition-modal', class extends HTMLElement {

      connectedCallback() {
        const modalElement = document.querySelector('ion-modal');

        const battle = modalElement.componentProps.battle;
        const winner = modalElement.componentProps.winner;

        this.innerHTML = `
        <ion-header>
          <ion-toolbar>
            <ion-buttons slot="primary">
              <ion-button id="dismissButton">
              <ion-icon slot="icon-only" name="close"></ion-icon>
              </ion-button>
            </ion-buttons>
              <ion-title>` +
          battle.user1.firstName + ` vs. ` +
          battle.user2.firstName + `</ion-title>
          </ion-toolbar>
        </ion-header>
        <ion-content>
          <br>
          <ion-label style="margin: 30px">Chancen: ` + battle.chance1 + `% zu ` + battle.chance2 + `%</ion-label>
            <ion-row id="row" style="margin: 25px; align-items: center">
              <ion-col style="max-width: 100px">Berechne...</ion-col>
              <ion-col>
                <ion-spinner name="crescent"></ion-spinner>
              </ion-col>
            </ion-row>
            <ion-row>
              <ion-label style="margin: 30px; visibility: hidden" id="winner">Gewinner: ` + winner.firstName + `</ion-label>
            </ion-row>
            <ion-row id="content">
            </ion-row>
        </ion-content>
        `;
      }
    });
  }

  /**
   * Handle challenge button
   * @param user User to be challenged
   */
  private challengeOpponent(user: User) {
    const battle = new Battle(UserService.getCurrentUser(), user);
    const winner = battle.execute();

    const modalElement = document.createElement('ion-modal');
    modalElement.component = 'competition-modal';
    modalElement.componentProps = { battle, winner };
    document.body.appendChild(modalElement);
    modalElement.present();

    console.log('[PLAYERS]\t' + battle.user1.loginName + ', ' + battle.user2.loginName);
    console.log('[WINNER]\t' + winner.loginName);

    setTimeout(() => {
      document.getElementById('dismissButton').onclick = () => document.querySelector('ion-modal').dismiss();

      document.getElementById('row').setAttribute('style', 'visibility: hidden');
      document.getElementById('winner').setAttribute('style', 'visibility: visible; margin: 30px');

      const content = document.getElementById('content');
      const image = document.createElement('img');

      if (winner.loginName === UserService.getCurrentUser().loginName) {
        image.src = '../../assets/images/winner.png';
      } else {
        image.src = '../../assets/images/loser.png';
      }
      image.setAttribute('style', 'width: 50%; height: 50%');
      content.appendChild(image);
    }, 3000);
  }

  /**
   * Creates user element with name, number of items, ready state of competition and challenge button
   * @param user User to be displayed
   */
  private createUserItem(user: User): void {
    // List item
    const ionItem: HTMLIonItemElement = document.createElement('ion-item');

    // IonCard
    const ionCard: HTMLIonCardElement = document.createElement('ion-card');
    ionItem.append(ionCard);

    // IonCardHeader
    const ionCardHeader: HTMLIonCardHeaderElement = document.createElement('ion-card-header');
    ionCard.appendChild(ionCardHeader);

    // IonCardHeaderTitle
    const ionCardTitle: HTMLIonCardTitleElement = document.createElement('ion-card-title');
    ionCardTitle.innerText = user.firstName + ' ' + user.lastName;
    ionCardHeader.appendChild(ionCardTitle);

    // IonCardContent
    const ionCardContent: HTMLIonCardContentElement = document.createElement('ion-card-content');
    ionCard.appendChild(ionCardContent);

    // IonGrid
    const ionGrid: HTMLIonGridElement = document.createElement('ion-grid');
    ionCardContent.appendChild(ionGrid);

    // First Row
    const ionGridRow: HTMLIonRowElement = document.createElement('ion-row');
    ionGrid.appendChild(ionGridRow);

    // First Column
    const ionGridCol: HTMLIonColElement = document.createElement('ion-col');
    ionGridCol.innerText = 'Anzahl Spieleritems: ' + user.player.items.length;
    ionGridCol.setAttribute('style', 'width: 300px');
    ionGridRow.appendChild(ionGridCol);

    const ionGridRow2: HTMLIonRowElement = document.createElement('ion-row');
    ionGridRow2.setAttribute('style', 'align-items: center');
    ionGrid.appendChild(ionGridRow2);

    // Second Column
    const ionGridColDown1: HTMLIonColElement = document.createElement('ion-col');
    ionGridColDown1.innerText = 'Bereit für einen Wettbewerb: ';
    ionGridColDown1.setAttribute('style', 'width: 300px');
    ionGridRow2.appendChild(ionGridColDown1);

    const ionGridColDown2: HTMLIonColElement = document.createElement('ion-col');
    ionGridColDown2.setAttribute('style', 'width: 300px');
    ionGridRow2.appendChild(ionGridColDown2);

    const button: HTMLIonButtonElement = document.createElement('ion-button');
    button.onclick = () => this.challengeOpponent(user);
    if (user.player.competitionReady) {
      button.innerText = 'Herausfordern';
      button.color = 'success';
    } else {
      button.innerText = 'Nicht bereit';
      button.disabled = true;
      button.color = 'medium';
    }
    ionGridColDown2.appendChild(button);

    const ionlist: HTMLIonListElement = document.getElementById('competitionList') as HTMLIonListElement;
    ionlist.appendChild(ionItem);
  }

  /**
   * Handle logout button
   */
  logout() {
    new LoginService().logout();
    this.navCtrl.navigateBack('login');
  }

}
