import { Component } from '@angular/core';
import { LoginService } from 'src/login/loginService';
import { NavController } from '@ionic/angular';

/**
 * Simple login page with two input fields and a button
 *
 * @author Tim Wenzel
 */
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage {

  constructor(private navCtrl: NavController) { }

  /**
   * Handle login button
   */
  login() {
    const loginName: string =
      (document.getElementById('login') as HTMLIonInputElement).value;
    const password: string =
      (document.getElementById('password') as HTMLIonInputElement).value;

    if (new LoginService().login(loginName, password)) {
      this.navCtrl.navigateRoot('tabs/dashboard');
    } else {
      const alert = document.createElement('ion-alert');
      alert.header = 'Ungültiger Login';
      alert.message = 'Diese Kombination von Login-Name und Passwort ist ungültig.\nBitte Angaben prüfen!';
      alert.buttons = ['OK'];

      document.body.appendChild(alert);
      return alert.present();
    }
  }

  /**
   * Handle reset application button
   */
  resetApplication() {
    localStorage.clear();
    location.reload();
  }

}
