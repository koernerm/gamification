import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { SonarqubeConnection } from 'src/model/metrics/sonarqubeConnection';
import { GitLabConnection } from 'src/model/vcs/gitLabConnection';
import { UserService } from 'src/login/userService';
import { Constants } from 'src/model/constants';
import { Item } from 'src/model/game/item';

/**
 * Page with evaluation criteria state
 *
 * @author Tim Wenzel
 */
@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
})
export class DetailsPage implements OnInit {

  /* Page / criterion title */
  title: string;
  /* Page / criterion icon */
  icon: string;

  /* Connection to version control system */
  vcsConnection;
  /* Connection to SonarQube service */
  sonarcubeConnection: SonarqubeConnection;

  constructor(private navCtrl: NavController) {
    if (!localStorage.getItem('types')) {
      this.navCtrl.back();
    }

    const set = JSON.parse(localStorage.getItem('types'));
    this.title = set[0];
    this.icon = set[2];

    localStorage.removeItem('types');

    this.vcsConnection = new GitLabConnection(Constants.PROJECT_ID_GITLAB, UserService.getCurrentUser().userToken);
    this.sonarcubeConnection = new SonarqubeConnection(Constants.SONARQUBE_HOST, Constants.SONARQUBE_PROJECT_ID);
  }

  /**
   * Evaluation of handling method for specific criterion
   */
  ngOnInit() {
    try {
      const currentUser = UserService.getCurrentUser();
      const userName = currentUser.firstName + ' ' + currentUser.lastName;

      switch (this.title) {
        case 'Commits':
          this.handleQuantityDetails(this.vcsConnection.getCommitsByUser(userName, 1, 0));
          break;
        case 'Issues':
          this.handleQuantityDetails(this.vcsConnection.getCompletedIssuesByUser(userName, 1, 0));
          break;
        case 'Merge Requests':
          this.handleQuantityDetails(this.vcsConnection.getMergeRequestsByUser(userName, 1, 0));
          break;
        case 'Merges':
          this.handleQuantityDetails(this.vcsConnection.getMergesByUser(userName, 1, 0));
          break;
        case 'Meilensteine':
          this.handleQuantityDetails(this.vcsConnection.getMilestonesByUser(userName, 1, 0));
          break;
        case 'Lines of Code':
          this.handleQuantityDetails(this.vcsConnection.getLocForUser(userName, 1, 0));
          break;
        case 'Bugs':
          this.handleQualityDetails(this.sonarcubeConnection.getBugsByUser(userName));
          break;
        case 'Security':
          this.handleQualityDetails(this.sonarcubeConnection.getSecurityByUser(userName));
          break;
        case 'Code Smells':
          this.handleQualityDetails(this.sonarcubeConnection.getCodeSmellsByUser(userName));
          break;
        case 'Code Duplications':
          this.handleQualityDetails(this.sonarcubeConnection.getCodeDuplicationsByUser(userName));
          break;
        case 'Zyklomatische Komplexität':
          this.handleQualityDetails(this.sonarcubeConnection.getCycloComplexityByUser(userName));
          break;
        case 'Kognitive Komplexität':
          this.handleQualityDetails(this.sonarcubeConnection.getCognitiveComplexityByUser(userName));
          break;
        default:
          throw Error('Default error');
      }
    } catch (e) {
      this.navCtrl.back();
      this.showAlert('Noch nicht implementiert...');
    }
  }

  /**
   * Handling of qualitiy criteria evaluation
   * @param method Specific criterion evaulation method
   */
  private handleQualityDetails(method): void {
    method.then(val => {
      document.getElementById('details-spinner').setAttribute('style', 'visibility: hidden');
      for (const t of Constants.TARGETS_QUALITY[this.title]) {
        this.createRowElement(this.title, t, val, 'gelöst');
      }
    }).catch(e => {
      if (e.message.includes('Authorization')) {
        this.navCtrl.back();
        this.showToast('GitLab-Token fehlt oder ist ungültig!', 'danger');
      } else if (e.message.includes('implemented')) {
        this.navCtrl.back();
        this.showAlert('Noch nicht implementiert...');
      } else {
        this.navCtrl.back();
        this.showToast('Fehler bei der Anfrage. Server nicht erreichbar.', 'danger');
      }
    });
  }

  /**
   * Handling of quantity criteria evaluation
   * @param method Specific criterion evaulation method
   */
  private handleQuantityDetails(method): void {
    method.then(val => {
      document.getElementById('details-spinner').setAttribute('style', 'visibility: hidden');
      for (const t of Constants.TARGETS_QUANTITY[this.title]) {
        this.createRowElement(this.title, t, val, 'erreicht');
      }
    }).catch(e => {
      if (e.message.includes('Authorization')) {
        this.navCtrl.back();
        this.showToast('GitLab-Token fehlt oder ist ungültig!', 'danger');
      } else if (e.message.includes('implemented')) {
        this.navCtrl.back();
        this.showAlert('Noch nicht implementiert...');
      } else {
        this.navCtrl.back();
        this.showToast('Fehler bei der Anfrage. Bitte nochmal versuchen.', 'danger');
      }
    });
  }

  /**
   * Creation of single row element with:
   * * Currently reached criterion value
   * * Target criterion value
   * * Progress bar
   * * Button to claim reward
   * @param criteria Name of criterion
   * @param targetNumber Target value
   * @param currentNumber Currently reached value
   * @param successString String to be displayed in element
   */
  private createRowElement(criteria: string, targetNumber: number, currentNumber: number, successString: string): void {
    const grid: HTMLIonGridElement = document.createElement('ion-grid');
    document.getElementById('details-content').appendChild(grid);

    const upperRow: HTMLIonRowElement = document.createElement('ion-row');
    upperRow.innerText = currentNumber.toLocaleString() + ' von ' + targetNumber.toLocaleString() + ' ' + criteria + ' ' + successString;
    grid.appendChild(upperRow);

    const lowerRow: HTMLIonRowElement = document.createElement('ion-row');
    lowerRow.setAttribute('style', 'align-items: center');
    grid.appendChild(lowerRow);

    const colLeft: HTMLIonColElement = document.createElement('ion-col');
    lowerRow.appendChild(colLeft);

    const progressBar: HTMLIonProgressBarElement = document.createElement('ion-progress-bar');
    progressBar.value = currentNumber / targetNumber;
    colLeft.appendChild(progressBar);

    const colRight: HTMLIonColElement = document.createElement('ion-col');
    lowerRow.appendChild(colRight);

    const claimButton: HTMLIonButtonElement = document.createElement('ion-button');
    claimButton.innerText = 'Belohnung';
    claimButton.size = 'small';
    claimButton.onclick = () => {
      let complexity;
      if (Constants.TARGETS_QUANTITY[criteria]) {
        complexity = this.evaluateComplexity(3, 6, Constants.TARGETS_QUANTITY[criteria].indexOf(targetNumber));
      } else if (Constants.TARGETS_QUALITY[criteria]) {
        complexity = this.evaluateComplexity(3, 6, Constants.TARGETS_QUALITY[criteria].indexOf(targetNumber));
      } else {
        throw new Error('Invalid criterion type.');
      }

      let item: Item;
      if (complexity === 1) {
        item = Item.getLowValueItem();
      } else if (complexity === 2) {
        item = Item.getMediumValueItem();
      } else {
        item = Item.getHighValueItem();
      }

      const user = UserService.getCurrentUser();
      user.player.items.push(item);
      user.player.rewards[this.title].push(targetNumber);
      UserService.updateUser(user);

      this.disableButton(claimButton, 'Erhalten');
      claimButton.setAttribute('color', 'success');

      this.showToast(item.name + ' [' + item.value.toFixed(2).toString() + ']');
    };

    if (currentNumber < targetNumber) {
      this.disableButton(claimButton, 'Belohnung');
    } else if (UserService.getCurrentUser().player.rewards[this.title].includes(targetNumber)) {
      this.disableButton(claimButton, 'Erhalten');
      claimButton.setAttribute('color', 'success');
    } else {
      claimButton.setAttribute('color', 'success');
    }

    colRight.appendChild(claimButton);
  }

  private disableButton(button: HTMLIonButtonElement, innerText: string): void {
    button.setAttribute('disabled', 'disabled');
    button.setAttribute('color', 'medium');
    button.innerText = innerText;
  }

  private showAlert(message: string) {
    const alert = document.createElement('ion-alert');
    alert.message = message;

    document.body.appendChild(alert);
    alert.present();
  }

  private showToast(message: string, color?: string) {
    const toast = document.createElement('ion-toast');
    toast.message = message;
    toast.duration = 1500;
    toast.color = color;
    toast.setAttribute('style', 'text-align: center');

    document.body.appendChild(toast);
    toast.present();
  }

  private evaluateComplexity(limitLowValue: number, limitMediumValue: number, evalValue: number): number {
    if (evalValue >= limitMediumValue) {
      return 3;
    } else if (evalValue >= limitLowValue) {
      return 2;
    } else {
      return 1;
    }
  }

}
