import { Component, OnInit } from '@angular/core';
import { User, UserService } from 'src/login/userService';
import { NavController } from '@ionic/angular';
import { LoginService } from 'src/login/loginService';
import { Constants } from 'src/model/constants';

/**
 * Progress overview tab
 *
 * @author Tim Wenzel
 */
@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {

  /* Name of current player to be displayed on top */
  userString: string;

  constructor(private navCtrl: NavController) {
    const currentUser: User = UserService.getCurrentUser();
    if (currentUser == null) {
      navCtrl.navigateBack('login');
    }
    this.userString = currentUser.firstName + ' ' + currentUser.lastName;
  }

  /**
   * Initialization of criteria lists
   */
  ngOnInit(): void {
    const quantityList: HTMLIonListElement = document.getElementById('progressList') as HTMLIonListElement;
    this.createCriteriaList(Constants.TYPES_QUANTITY, quantityList);

    const qualityList: HTMLIonListElement = document.getElementById('quality-list') as HTMLIonListElement;
    this.createCriteriaList(Constants.TYPES_QUALITY, qualityList);
  }

  /**
   * Creation of single list
   * @param types List of items
   * @param list HTML element to store items in
   */
  private createCriteriaList(types: any[], list: HTMLElement): void {
    types.forEach(set => {
      const item = document.createElement('ion-item');
      item.setAttribute('button', 'button');
      item.setAttribute('detail', 'true');
      item.onclick = () => {
        if (!document.querySelector('ion-alert')) {
          localStorage.setItem('types', JSON.stringify(set));
          this.navCtrl.navigateForward('details');
        }
      };
      list.appendChild(item);

      const icon = document.createElement('ion-icon');
      icon.setAttribute('name', set[2]);
      item.appendChild(icon);

      const label = document.createElement('ion-label');
      label.setAttribute('style', 'margin-left: 20px');
      label.innerText = set[0];
      item.append(label);

      const chip = document.createElement('ion-chip');
      chip.innerText = '?';
      chip.onclick = () => this.showAlert(set[0], set[1]);
      chip.setAttribute('button', 'button');
      chip.setAttribute('mode', 'ios');
      chip.setAttribute('style', 'margin-right: 20px');
      chip.setAttribute('color', 'primary');
      item.appendChild(chip);
    });
  }

  /**
   * Alert method
   * @param element Name of criterion
   * @param description Description of criterion
   */
  private showAlert(element: string, description: string) {
    const alert = document.createElement('ion-alert');
    alert.header = element;
    alert.message = description;
    alert.buttons = ['Verstanden'];

    document.body.appendChild(alert);
    return alert.present();
  }

  /**
   * Handle logout button
   */
  logout() {
    new LoginService().logout();
    this.navCtrl.navigateBack('login');
  }

}
