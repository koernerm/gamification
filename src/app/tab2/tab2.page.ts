import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { User, UserService } from 'src/login/userService';
import { LoginService } from 'src/login/loginService';
import { Item } from 'src/model/game/item';

/**
 * Dashboard / overview tab
 *
 * @author Tim Wenzel
 */
@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit {

  /* Name of current player to be displayed on top */
  userString: string;
  /* List of collected items / items to display */
  items: Item[];

  constructor(private navCtrl: NavController) {
    const currentUser: User = UserService.getCurrentUser();
    if (currentUser == null) {
      navCtrl.navigateBack('login');
    }
    this.userString = currentUser.firstName + ' ' + currentUser.lastName;
    this.items = currentUser.player.items;
  }

  /**
   * Initialization of dashboard items
   */
  ngOnInit(): void {
    const toggle: HTMLIonToggleElement = document.getElementById('toggle') as HTMLIonToggleElement;
    toggle.checked = UserService.getCurrentUser().player.competitionReady;

    (document.getElementById('token') as HTMLIonInputElement).value = UserService.getCurrentUser().userToken;

    const segment = document.querySelector('ion-segment');
    segment.addEventListener('ionChange', ev => {
      this.sortAndShowElements((ev as CustomEvent).detail.value);
    });

    this.sortAndShowElements('name');
  }

  /**
   * Handle token registration via input field
   */
  registerToken() {
    const users: User[] = UserService.getUsers();
    users.forEach(u => {
      if (u.loginName === UserService.getCurrentUser().loginName) {
        users[users.indexOf(u)].userToken = (document.getElementById('token') as HTMLIonInputElement).value;
      }
    });
    UserService.setUsers(users);

    const toast = document.createElement('ion-toast');
    toast.message = 'Token registriert';
    toast.buttons = ['close'];
    toast.duration = 1500;
    toast.color = 'medium';
    toast.style.textAlign = 'center';

    document.body.appendChild(toast);
    toast.present();
  }

  /**
   * Handle logout button
   */
  logout() {
    new LoginService().logout();
    this.navCtrl.navigateBack('login');
  }

  /**
   * Handle toggle element
   */
  toggleButton() {
    const toggle: HTMLIonToggleElement = document.getElementById('toggle') as HTMLIonToggleElement;

    const users: User[] = UserService.getUsers();
    users.forEach(user => {
      if (UserService.getCurrentUser().loginName === user.loginName) {
        user.player.competitionReady = !toggle.checked;
      }
    });

    UserService.setUsers(users);
  }

  private sortAndShowElements(sortBy: string) {
    const items = UserService.getCurrentUser().player.items;
    const list: HTMLIonListElement = document.getElementById('item-list') as HTMLIonListElement;
    while (list.lastElementChild) {
      list.removeChild(list.lastElementChild);
    }

    if (sortBy === 'name') {
      items.sort((itemA, itemB) => itemA.value - itemB.value);
      items.sort((itemA, itemB) => itemA.name.localeCompare(itemB.name));
    } else {
      items.sort((itemA, itemB) => itemA.name.localeCompare(itemB.name));
      items.sort((itemA, itemB) => itemA.value - itemB.value);
    }
    items.forEach(item => {
      const itemElement = document.createElement('ion-item');
      list.appendChild(itemElement);

      const colImage = document.createElement('ion-col');
      colImage.setAttribute('size', '2');
      itemElement.appendChild(colImage);

      const image = document.createElement('img');
      image.src = item.iconPath;
      image.setAttribute('style', 'max-height: 50px');
      colImage.appendChild(image);

      const colName = document.createElement('ion-col');
      colName.setAttribute('size', '6');
      colName.innerText = item.name;
      itemElement.appendChild(colName);

      const colValue = document.createElement('ion-col');
      colValue.innerText = item.value.toFixed(2).toString();
      itemElement.appendChild(colValue);
    });
  }

}
