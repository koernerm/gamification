import { Item } from './item';

/**
 * User's representation in the game
 * 
 * @author Tim Wenzel
 */
export class Player {

    /* Wants to compete with others? */
    competitionReady: boolean;

    /* List of collected items */
    items: Item[];

    /* Already earned rewards */
    rewards: {};

    constructor() {
        this.items = [];
        this.competitionReady = false;

        /* Categories of rewards with list of earned values */
        this.rewards = {
            'Commits': [],
            'Issues': [],
            'Merge Requests': [],
            'Merges': [],
            'Meilensteine': [],
            'Lines of Code': [],
            'Bugs': [],
            'Security': [],
            'Code Smells': [],
            'Code Duplications': [],
            'Zyklomatische Komplexität': [],
            'Kognitive Komplexität': []
        };
    }

}
