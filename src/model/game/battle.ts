import { Player } from './player';
import { User } from 'src/login/userService';

/**
 * Gamification battle between two users.
 *
 * @author Tim Wenzel
 */
export class Battle {

    user1: User;
    user2: User;
    date: Date;
    chance1: number;
    chance2: number;

    constructor(user1: User, user2: User) {
        this.user1 = user1;
        this.user2 = user2;
        this.date = new Date();
    }

    /**
     * Battle execution method.
     * Chances to win at beginning: 50%.
     * Final chances determined by item amount and values.
     */
    execute(): User {
        const player1: Player = this.user1.player;
        const player2: Player = this.user2.player;

        let chancePlayer1 = 0.5;
        player1.items.forEach(item => chancePlayer1 += item.value);

        let chancePlayer2 = 0.5;
        player2.items.forEach(item => chancePlayer2 += item.value);

        const sum = chancePlayer1 + chancePlayer2;
        chancePlayer1 = chancePlayer1 / sum;
        chancePlayer2 = chancePlayer2 / sum;

        this.chance1 = Math.round(chancePlayer1 * 1000) / 10;
        this.chance2 = Math.round(chancePlayer2 * 1000) / 10;

        console.log('[CHANCES]\t' +
            Math.round(chancePlayer1 * 1000) / 10 + '% : ' +
            Math.round(chancePlayer2 * 1000) / 10 + '%');

        return Math.random() < chancePlayer1 ? this.user1 : this.user2;
    }

    /**
     * Returns date as string representation.
     */
    getDateString(): string {
        return (this.date.getDay + '.' + this.date.getMonth + '.' + this.date.getFullYear);
    }

    /**
     * Returns time as string representation.
     */
    getTimeString(): string {
        return (this.date.getHours + '.' + this.date.getMinutes + '.' + this.date.getSeconds);
    }

    /**
     * Returns timestamp as string representation.
     */
    getTimeStamp(): string {
        return (this.getDateString() + ' | ' + this.getTimeString());
    }

}
