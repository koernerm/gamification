/**
 * Constants for the gamification app
 * 
 * @author Tim Wenzel
 */
export class Constants {

    /* Default timeout for API requests */
    static readonly DEFAULT_TIMEOUT = 1000;

    /* Default GitLab project id (points to THIS project for prototype purpose) */
    static readonly PROJECT_ID_GITLAB = 17980335;

    // First items of TYPES_QUANTITY and keys of TARGETS_QUANTITY should be the same!
    static readonly TYPES_QUANTITY = [
        ['Commits', 'Anzahl der von einem Nutzer durchgeführten Commits.', 'git-commit'],
        ['Issues', 'Anzahl der von einem Nutzer erfolgreich abgeschlossenen Issues.', 'checkbox-outline'],
        ['Merge Requests', 'Anzahl der von einem Nutzer bearbeiteten Merge Requests.', 'git-pull-request'],
        ['Merges', 'Anzahl von Merge Requests eines Nutzers, die ohne Review-Fehler bestätigt werden.', 'git-merge'],
        ['Milestones', 'Anzahl der erfolgreich abgeschlossenen Meilensteine, an denen ein Nutzer beteiligt war.', 'done-all'],
        ['Lines of Code', 'Anzahl der geänderten und hinzugefügten Codezeilen eines Nutzers.', 'reorder']
    ];
    static readonly TARGETS_QUANTITY = {
        // Strings are necessary as key here - don't believe lint!
        'Commits': [1, 30, 50, 100, 150, 200, 250, 300],
        'Issues': [1, 5, 10, 15, 20, 25, 30, 50],
        'Merge Requests': [1, 5, 10, 15, 20, 25, 30, 50],
        'Merges': [1, 3, 5, 10, 15, 20, 25, 30],
        'Milestones': [1, 3, 5, 10, 15, 20, 25, 30],
        'Lines of Code': [100, 500, 1_000, 5_000, 10_000, 20_000, 50_000, 100_000]
    };

    static readonly TYPES_QUALITY = [
        ['Bugs', 'Anzahl der von einem Nutzer behobenen Fehler.', 'bug'],
        ['Security', 'Anzahl der von einem Nutzer behobenen Sicherheitsproblemen.', 'lock'],
        ['Code Smells', 'Anzahl der von einem Nutzer behobenen Code Smells.', 'nuclear'],
        ['Code Duplications', 'Anzahl der von einem Nutzer aufgelösten Code-Duplizierungen.', 'copy'],
        ['Zyklomatische Komplexität', 'Anzahl der vereinfachten Codestellen bezogen auf die McCabe-Komplexität.', 'podium'],
        ['Kognitive Komplexität', 'Anzahl der vereinfachten Codestellen bezogen auf die Kognitive Komplexität.', 'analytics']
    ];
    static readonly TARGETS_QUALITY = {
        // Strings are necessary as key here - don't believe lint!
        'Bugs': [1, 5, 10, 20, 30, 50, 75, 100],
        'Security': [1, 3, 5, 10, 15, 20, 25, 30],
        'Code Smells': [1, 5, 10, 20, 30, 50, 75, 100],
        'Code Duplications': [1, 3, 5, 10, 15, 20, 25, 30],
        'Zyklomatische Komplexität': [999],
        'Kognitive Komplexität': [999]
    };

    /* SonarQube default host IP (local with port) */
    static readonly SONARQUBE_HOST = 'localhost:9000';
    /* SonarQube default project ID */
    static readonly SONARQUBE_PROJECT_ID = 'Gamification';

    /* Low value items with icon name */
    static readonly ITEMS_LOW = [
        ['Brot', 'bread'],
        ['Buch', 'book'],
        ['Angel', 'fishing_rod'],
        ['Flasche', 'bottle'],
        ['Kohle', 'coal']
    ];

    /* Medium value items with icon name */
    static readonly ITEMS_MEDIUM = [
        ['Helm', 'helmet'],
        ['Hose', 'leggings'],
        ['Trank', 'potion'],
        ['Eisen', 'iron'],
        ['Gold', 'gold']
    ];

    /* High value items with icon name */
    static readonly ITEMS_HIGH = [
        ['Bogen', 'bow'],
        ['Pfeil', 'arrow'],
        ['Schwert', 'sword'],
        ['Diamant', 'diamond'],
        ['Smaragd', 'emerald']
    ];

    /* Strings to be rejected during commit message filtering */
    static readonly REJECTED_STRINGS = ['Initial commit', 'SonarQube']

}
